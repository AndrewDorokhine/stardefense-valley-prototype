﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayTrackerUI : MonoBehaviour
{
    [SerializeField] Text _dayTracker;
    private int currentDay;

    // Start is called before the first frame update
    void Start()
    {
        currentDay = 0;
        _dayTracker.text = "";
    }

    public void IncrementDay()
    {
        currentDay += 1;
        _dayTracker.text = "Day " + currentDay;
        if(currentDay == 11)
        {
            // Very basic "game victory" code. Will just raise a "game victory" event in the future.
            // Game ends on the morning of day 11.
            Debug.Log("U DID IT!! VICTORRY SCREEEEEECH!");
            Time.timeScale = 0f;
        }
    }
}
