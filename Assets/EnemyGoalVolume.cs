﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGoalVolume : MonoBehaviour
{
    [SerializeField] private Vector3 _size;

    [SerializeField] private LayerMask _layerMask;
    // Update is called once per frame
    void Update()
    {
        Collider[] results = new Collider[16];
        int hitCount =
            Physics.OverlapBoxNonAlloc(transform.position, _size * 0.5f, results, transform.rotation, _layerMask);
        
        if (hitCount > 0)
        {
            for (int i = 0, length = hitCount; i < length; i++)
            {
                EnemyBase enemyBase = results[i].GetComponent<EnemyBase>();
                if (enemyBase)
                    enemyBase.Explode();
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.matrix = Matrix4x4.TRS(transform.TransformPoint(Vector3.zero), transform.rotation, Vector3.one);
        Gizmos.DrawWireCube(Vector3.zero, _size);
    }
}
