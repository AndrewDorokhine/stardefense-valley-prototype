using Cinemachine;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// A simple character controller that can be easily modified to suit your needs.  Movement is 
/// </summary>
[RequireComponent(typeof(CharacterController))]
public class Player : MonoBehaviour
{
    [SerializeField, Tooltip("Max speed.")] private float _speed = 7f;
    [SerializeField, Tooltip("Approximate time to reach max speed.")] private float _accelerationTime = 0.1f;
    private float _accelerationVelocity;
    [SerializeField, Tooltip("Approximate time to reach target rotation when turning.")] private float _turnSpeedTime = 1f;
    private float _turnSpeedVelocity;
    private float _currentSpeed;
    private float _velocityY;
    private CharacterController _characterController;
    private Vector3 _input;
    private Vector3 _rightStickInput;
    float camRayLength = 100f;
    
    [SerializeField] private Transform _playerCameraTransform;
    
    void Start()
    {
        _characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        ReadInput();
        Turning();
        //CalculateRotation();
        CalculateSpeed();
        Move();
    }

    /// <summary>
    /// Read input from whatever source, this can be altered to take AI input
    /// </summary>
    private void ReadInput()
    {
        _input.x = Input.GetAxisRaw("LeftHorizontal");
        _input.y = Input.GetAxisRaw("LeftVertical");
        _input.Normalize();

        _rightStickInput.x = Input.GetAxisRaw("RightHorizontal");
        _rightStickInput.y = Input.GetAxisRaw("RightVertical");
        _rightStickInput.Normalize();
    }

    void Turning()
    {
        // Create a ray from the mouse cursor on screen in the direction of the camera.
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Create a RaycastHit variable to store information about what was hit by the ray.
        RaycastHit floorHit;
        // Perform the raycast and if it hits something on the floor layer...
        //if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask)) // See how this works without a floor mask
        if (Physics.Raycast(camRay, out floorHit, camRayLength))
        {
            // Create a vector from the player to the point on the floor the raycast from the mouse hit.
            Vector3 playerToMouse = floorHit.point - transform.position;

            // Ensure the vector is entirely along the floor plane.
            playerToMouse.y = 0f;

            // Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            // Set the player's rotation to this new rotation.
            //playerRigidbody.MoveRotation(newRotation);
            transform.eulerAngles = newRotation.eulerAngles;
        }
    }

    /// <summary>
    /// Figure out how to handle rotating the character each frame.  You could change this to have the controller look
    /// at the mouse or towards the right stick input
    /// </summary>
    //private void CalculateRotation()
    //{
    //    if (_rightStickInput != Vector3.zero)
    //    {
    //        float targetRotation = Mathf.Atan2 (_rightStickInput.x, _rightStickInput.y) * Mathf.Rad2Deg + _playerCameraTransform.eulerAngles.y;
    //        transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref _turnSpeedVelocity, _turnSpeedTime);
    //    }
    //}

    /// <summary>
    /// Handle acceleration/deceleration
    /// </summary>
    private void CalculateSpeed()
    {
        float targetSpeed = _speed * _input.normalized.magnitude;
        _currentSpeed = Mathf.SmoothDamp(_currentSpeed, targetSpeed, ref _accelerationVelocity, _accelerationTime);
    }
    
    /// <summary>
    /// Calculate velocity and actually move the character controller.
    /// </summary>
    private void Move()
    {
        _velocityY += Physics.gravity.y * Time.deltaTime;
        
        Vector3 relativeDirection = _playerCameraTransform.TransformDirection(new Vector3(_input.x, 0, _input.y));
        relativeDirection.y = 0;
        relativeDirection.Normalize();
        
        Vector3 velocity =  relativeDirection * _currentSpeed;
        
        velocity.y += _velocityY;
        _characterController.Move(velocity * Time.deltaTime);
        
        Vector3 controllerVelocity = _characterController.velocity;
        _currentSpeed = new Vector2(controllerVelocity.x, controllerVelocity.z).magnitude;
        _velocityY = _characterController.isGrounded ? 0 : _velocityY;
    }

    public float GetCurrentSpeed()
    {
        return _currentSpeed;
    }

    /// <summary>
    /// Character controllers get a different callback than rigidbodies but this works the same way.
    /// </summary>
    /// <param name="hit"></param>
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {

    }
}
