﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TowerGrower : MonoBehaviour
{
    [SerializeField] float _growthTime;
    [SerializeField] Material _gunFullyGrownMat;

    // Towers instantiate without the Pickupable component, which keeps the player from interacting with them
    // Once towers are "ripe," they become pickupable.
    void Start()
    {
        gameObject.transform.DOScale(2, _growthTime).OnComplete(MakePickupable);
    }

    private void MakePickupable()
    {
        gameObject.GetComponent<MeshRenderer>().material = _gunFullyGrownMat;
        Pickupable makePickupable = GetComponent<Pickupable>();
        makePickupable.enabled = true;
    }
}
