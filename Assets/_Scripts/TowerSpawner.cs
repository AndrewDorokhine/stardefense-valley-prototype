﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerSpawner : MonoBehaviour
{
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private GameObject _prefab;

    [SerializeField] private float towerSpawnTimer;
    private float countUpTimer = 0;

    void Start()
    {
        Instantiate(_prefab, _spawnPoint);
    }

    // Each spawner can only hold one tower at a time. A timer will start when the tower is picked
    // up, and then a new tower will "grow."
    void Update()
    {
        if (transform.childCount == 0)
        {
            countUpTimer += Time.deltaTime;

            if (countUpTimer >= towerSpawnTimer)
            {
                Instantiate(_prefab, _spawnPoint);
                countUpTimer = 0;
            }
        }
    }
}
