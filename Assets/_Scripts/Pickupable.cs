﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class Pickupable : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private int _baseThrowMultiplier; // All throws are multiplied by this
    [SerializeField] private int _velocityMultiplier; // Adds to the throw power based on player's current movement speed
    [SerializeField] private int _gravityMultiplier; // How heavy the tower is when it's thrown.

    private bool _pickedUp = false;

    GameObject player;

    void OnEnable()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0) && _pickedUp)
        {
            ThrowTower();
        }
    }

    void FixedUpdate()
    {
        if (gameObject.GetComponent<Rigidbody>())
        {
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            rb.AddForce(new Vector3(0, Physics.gravity.y * _gravityMultiplier, 0));
        }
    }

    private void ThrowTower()
    {
        gameObject.transform.parent = null;
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        rb.isKinematic = false;
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        rb.AddForce((player.transform.forward * _baseThrowMultiplier) +
            (player.transform.forward * (player.GetComponent<Player>().GetCurrentSpeed() * _velocityMultiplier)));
        TogglePickedUp();
    }

    private void TogglePickedUp()
    {
        _pickedUp = !_pickedUp;
    }

    // Picking up a tower should only be possible within a certain close range (?)
    // Right now one can be picked up from anywhere.
    // The player can also hold more than one tower at a time.
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right && !_pickedUp)
        {
            ShootingAbility startShooting = GetComponentInChildren<ShootingAbility>();
            startShooting.enabled = true;
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
            gameObject.transform.parent = player.transform;
            gameObject.transform.DOMove(new Vector3(player.transform.position.x, player.transform.position.y + 3,
                player.transform.position.z), 0.5f).OnComplete(TogglePickedUp);

            // Code meant to make towers of all sizes go consistently to the top of the player's head. Not working yet.
            //pickUpTower.Append(gameObject.transform.DOMove(new Vector3(player.position.x,
            //    (player.GetComponent<Renderer>().bounds.size.y * 2), player.position.z), 0.5f));
        }
    }
}
