﻿using System.Collections;
using System.Collections.Generic;
using Noodlepop.GameEvents;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBase : MonoBehaviour
{
    private Transform _target;
    [SerializeField] private GameEvent _enemyReachedGoal;
    private NavMeshAgent _navMeshAgent;
    void Start()
    {
        if (_target == null)
            _target = GameObject.Find("Goal").transform;
        
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _navMeshAgent.destination = _target.position;

    }

    public void Explode()
    {
        _enemyReachedGoal.Raise();
        Destroy(gameObject);
    }
}
