﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController _characterController;
    private Vector3 _velocity = new Vector3();

    private void Start()
    {
        _characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        if (_characterController.isGrounded)
        {
            _velocity.y = -0.1f;
        }
        else
        {
            _velocity.y += Physics.gravity.y * Time.deltaTime;
        }

        _characterController.Move(_velocity);
    }
}
