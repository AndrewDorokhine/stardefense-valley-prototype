﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private GameObject _prefab;

    public void Spawn()
    {
        Instantiate(_prefab, _spawnPoint);
    }
}
