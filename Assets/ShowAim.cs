﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowAim : MonoBehaviour
{
    [SerializeField] private Transform _aimStart;
    [SerializeField] private Transform _player;
    [SerializeField] private float _range;
    [SerializeField] private LayerMask _layerMask;
    private LineRenderer _lineRenderer;
    
    // Start is called before the first frame update
    void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 lineStart = _aimStart.position;
        Vector3 lineEnd = _aimStart.position + (_range * _player.forward);
        RaycastHit[] results = new RaycastHit[16];

        int hitCount = Physics.RaycastNonAlloc(
            _aimStart.position,
            _player.forward,
            results,
            _range,
            _layerMask,
            QueryTriggerInteraction.Ignore);
            
        if (hitCount > 0)
        {
            RaycastHit nearestHit = results[0];
            for (int i = 0; i < hitCount; i++)
            {
                if (nearestHit.distance > results[i].distance)
                {
                    nearestHit = results[i];
                }
            }
                
            lineEnd = nearestHit.point;
        }
        
        _lineRenderer.SetPosition(0, lineStart);
        _lineRenderer.SetPosition(1, lineEnd);
    }
}
