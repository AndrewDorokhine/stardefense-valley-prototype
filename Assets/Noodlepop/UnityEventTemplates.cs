﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Noodlepop;

// Templates for UnityEvents.

#region Game specific types:
#endregion

#region Noodlepop Unity Events
#endregion

#region Primitives and Unity types:
[Serializable] public class UnityEventVector3 : UnityEvent<Vector3> { }
[Serializable] public class UnityEventBool : UnityEvent<bool> { }
[Serializable] public class UnityEventString : UnityEvent<string> { }
[Serializable] public class UnityEventInt : UnityEvent<int> { }
[Serializable] public class UnityEventRay : UnityEvent<Ray> { }
#endregion