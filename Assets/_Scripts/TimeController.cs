﻿using Noodlepop.FiniteStateMachine;
using Noodlepop.GameEvents;
using UnityEngine;
using UnityEngine.Events;

public class TimeController : MonoBehaviour
{
    public int StartingHour;
    public int FirstHourOfDay;
    public int FirstHourOfNight;
    
    [Space(10)]
    [SerializeField] private GameEvent _dayStateEvent;
    [SerializeField] private GameEvent _nightStateEvent;
    private bool _isDay;
    
    private int _hour;
    private int _minute;
    private float _time;
    
    [Space(10)]
    [SerializeField] private GameClock _clock = new GameClock();

    public void Start()
    {
        _nightStateEvent.Raise();
        _clock.FastForwardTime(StartingHour);
    }

    private void Update()
    {
        _clock.Tick(Time.deltaTime);
        _hour = _clock.Hour;
        _minute = _clock.Minute;
        _time = _clock.Time;

        if (_hour == FirstHourOfDay && !_isDay)
        {
            _dayStateEvent.Raise();
            _isDay = true;
        }

        if (_hour == FirstHourOfNight && _isDay)
        {
            _nightStateEvent.Raise();
            _isDay = false;
        }   
    }
}

[System.Serializable]
public class GameClock
{
    /// <summary>
    /// the current hour
    /// </summary>
    public int Hour { get; private set; }
    
    /// <summary>
    /// the current minute
    /// </summary>
    public int Minute { get; private set; }
    
    /// <summary>
    /// the current time
    /// </summary>
    public float Time => _timer;

    [SerializeField] private string _readableTime;
    
    [SerializeField, UnityEngine.Range(0, 60)] private int _realSecondsPerHour = 30;
    [SerializeField, UnityEngine.Range(0, 5)] private float _clockSpeed = 1f;
    [SerializeField] private float _hoursPerDay;
    
    private float _timer;

    public GameClock()
    {
        Hour = 0;
    }
    
    public void Tick(float deltaTime)
    {
        _timer += deltaTime * _clockSpeed;
        Hour = (int)(_timer / _realSecondsPerHour);
        Minute = (int) (_timer % _realSecondsPerHour);

        _readableTime = $"{Hour}h:{Minute}m";
        
        if (Hour == (int)_hoursPerDay + 1)
            _timer = 0;
    }

    public void FastForwardTime(int hour)
    {
        _timer = hour * _realSecondsPerHour;
    }
}