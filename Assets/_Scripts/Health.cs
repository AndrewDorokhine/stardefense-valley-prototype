﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    [SerializeField] private int _health = 15;
    [SerializeField] private UnityEvent _onDamage;
    [SerializeField] private UnityEvent _onDeath;
    //[SerializeField] private Text _healthText;

    private int _maxHealth;

    void Start()
    {
        _maxHealth = _health;
       // _healthText.text = _health + "/" + _maxHealth;
    }

    public void Damage()
    {
        _onDamage.Invoke();
        _health -= 1;
        //_healthText.text = _health + "/" + _maxHealth;
        if (_health < 0)
        {
            _onDeath.Invoke();
            Destroy(gameObject);
        }
    }
}
