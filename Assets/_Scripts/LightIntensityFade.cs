﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightIntensityFade : MonoBehaviour
{
    private Light _light;
    private float _targetIntensity;
    private bool _fading;

    private void Start()
    {
        _light = GetComponent<Light>();
    }

    void Update()
    {
        if (_fading)
        {
            _light.intensity = Mathf.MoveTowards(_light.intensity, _targetIntensity, Time.deltaTime);
            if (_light.intensity.Equals(_targetIntensity))
            {
                _fading = false;
            }
        }
    }

    public void Fade(float intensity)
    {
        _fading = true;
        _targetIntensity = intensity;
    }

}
