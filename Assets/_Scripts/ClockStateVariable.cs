﻿using System.Collections;
using System.Collections.Generic;
using Noodlepop.VariableAssets;
using UnityEngine;

[CreateAssetMenu]
public class ClockStateVariable : VariableAsset<ClockState>
{

}