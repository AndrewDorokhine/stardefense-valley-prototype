﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(LineRenderer))]
public class ShootingAbility : MonoBehaviour
{
    [SerializeField] private UnityEvent _onShoot;
    [SerializeField] private float _cooldown;
    [SerializeField] private Transform _rayStartTransform;
    private float _cooldownTimer;
    [SerializeField] private float _range;
    [SerializeField] private LayerMask _layerMask;
    [Space(10)]
    [SerializeField] private Color _rayColor;

    [SerializeField] private Light _muzzleFlash;
    [SerializeField] private ParticleSystem _particleSystem;
    private LineRenderer _lineRenderer;
    private Vector3 _particleStartPosition;

    void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.material = new Material(Shader.Find("Mobile/Unlit"));
        _particleStartPosition = _particleSystem.transform.localPosition;
    }

    // Tower now has the shooting ability, not the player.
    void Update()
    {
        _cooldownTimer += Time.deltaTime;

        if (_cooldownTimer > _cooldown) // Get a buffer of potential targets each time the tower is ready to fire - not every frame.
        {
            _cooldownTimer = 0f;
            Vector3 lineStart = _rayStartTransform.position;
            Vector3 lineEnd = _rayStartTransform.position + (_range * transform.forward);
            Collider[] inRangeEnemies = new Collider[32]; // Changing to 32 seems to remove the problem of skipping closer targets
            RaycastHit[] results = new RaycastHit[32];

            int hitCount = Physics.OverlapSphereNonAlloc(
                _rayStartTransform.position,
                _range,
                inRangeEnemies,
                _layerMask,
                QueryTriggerInteraction.Ignore);

            if (hitCount > 0)
            {
                //Collider targetedEnemy = inRangeEnemies[0];
                //float nearestDistance = (gameObject.transform.position - targetedEnemy.transform.position).magnitude;

                // Using object distances, find which one is the nearest before raycasting to it.
                float nearestDistance = Mathf.Infinity;

                for (int i = 0; i < hitCount; i++)
                {
                    float distance = (gameObject.transform.position - inRangeEnemies[i].transform.position).magnitude;
                    if (nearestDistance > distance)
                    {
                        nearestDistance = distance;
                        //targetedEnemy = inRangeEnemies[i];
                        gameObject.transform.LookAt(inRangeEnemies[i].transform);
                    }
                }

                //Collider targetedEnemy = inRangeEnemies[Random.Range(0, hitCount)];
                //gameObject.transform.LookAt(targetedEnemy.transform);

                if (Physics.Raycast(gameObject.transform.position, transform.forward, out RaycastHit enemyHit, _range, _layerMask,
                    QueryTriggerInteraction.Ignore) && enemyHit.transform.gameObject.layer == 10)
                {
                    lineEnd = enemyHit.point;

                    Health health = enemyHit.transform.gameObject.GetComponent<Health>();
                    _particleSystem.transform.position = lineEnd;
                    _particleSystem.Emit(1);

                    if (health)
                        health.Damage();

                    _lineRenderer.material = new Material(Shader.Find("Unlit/Color"));
                    _lineRenderer.material.color = _rayColor;

                    StartCoroutine(ShowFX(lineStart, lineEnd));
                    _particleSystem.transform.localPosition = _particleStartPosition;
                    _particleSystem.Emit(1);

                    Debug.DrawRay(_rayStartTransform.position, transform.forward * _range);
                    _onShoot.Invoke();
                }
            }
        }
    }

    IEnumerator ShowFX(Vector3 lineStart, Vector3 lineEnd)
    {
        _muzzleFlash.color = _rayColor;
        _muzzleFlash.enabled = true;
        _lineRenderer.enabled = true;
        _lineRenderer.SetPosition(0, lineStart);
        _lineRenderer.SetPosition(1, lineEnd);
        yield return new WaitForSeconds(0.05f);
        _lineRenderer.enabled = false;
        _muzzleFlash.enabled = false;
    }
}
